"use strict";
function isCheckNumber(number) {
    while((isNaN(number))||(number == null)||(number<0)||(!number)) {
        number = prompt('ERROR! Enter your number:', number);
    }  
    return +number;
}
function isFactorial(number) {
   if((number == 0) || (number == 1)) return 1;
   else return number * isFactorial(number - 1);
}
let outputFactorial, getNumber = prompt('Enter your number:');
getNumber = isCheckNumber(getNumber);
outputFactorial = isFactorial(getNumber);
alert(`Factorial numbers ${getNumber} = ` + outputFactorial);